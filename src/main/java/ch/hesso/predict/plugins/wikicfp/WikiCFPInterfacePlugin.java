/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.wikicfp;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.wikicfp.extractor.WikiCFPExtractor;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.plugins.PluginInterface;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WikiCFPInterfacePlugin extends PluginInterface {

	private final Set<APIEntityVisitor> _visitors = new HashSet<> ();

	@CallableMethod ( value = "Extract CFP",
			description = "Performs WikiCFP extraction and synchronization with triplestore. Matching conferences is done through title and/or acronym." )
	public void extractCfp () {
		WikiCFPExtractor extractor = new WikiCFPExtractor ();
		extractor.run ();
	}

	@Override
	public String name () {
		return "WikiCFP Call for Paper Extractor";
	}

	@Override
	public String description () {
		return "Extracts all Call for Papers available on <a href='http://www.wikicfp.com/cfp' target='_blank'>WikiCFP</a> website. Performs a deep extraction of each call entry (date, meta-data, text proposition).";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		return new HashMap<> ();
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return _visitors;
	}
}
