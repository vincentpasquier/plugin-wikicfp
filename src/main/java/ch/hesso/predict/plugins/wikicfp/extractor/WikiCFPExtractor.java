/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.wikicfp.extractor;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.CFPClient;
import ch.hesso.predict.client.resources.ConferenceClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.wikicfp.WikiCFPUtils;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WikiCFPExtractor {

	private final Multimap<String, String> cfpLinksWithCategories
			= HashMultimap.create ();

	private final Map<String, String> links = new HashMap<> ();

	private final WebPageClient client = WebPageClient.create ( APIClient.REST_API );

	private Set<Cfp> cfpToPublish = new HashSet<> ();

	private PluginInterface.PluginStatus.Builder builder =
			PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );

	/**
	 *
	 */
	public WikiCFPExtractor () {
	}

	public void run () {

		boolean success = extractCategories ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}

		builder.progress ( 1 );
		builder.totalProgress ( 4 );

		success = extractLinksFromCategories ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract link categories properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );

		success = extractCFPPage ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract CFP pages properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 3 );

		success = publishCFP ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to publish CFP properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 4 );

		success = commitToConferences ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to commit CFP to conferences properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Successfully committed CFP." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	// Not really progress bar worthy
	private boolean extractCategories () {
		boolean success = true;

		links.putAll ( WikiCFPUtils.extractCategoriesLinks () );

		return success;
	}

	private boolean extractLinksFromCategories () {
		boolean success = true;
		builder.text ( "Extracting CFP links from <a href='http://www.wikicfp.com/cfp/allcat' target='_blank'>category page</a> in a single pass. Gets all links and category associated with it." );
		PluginInterface.publishPluginStatus ( builder.build () );

		for ( Map.Entry<String, String> entry : links.entrySet () ) {
			Set<String> categoryLinks = WikiCFPUtils.extractCFPLinks ( entry.getValue () );
			for ( String link : categoryLinks ) {
				cfpLinksWithCategories.put ( link, entry.getKey () );
			}
		}

		return success;
	}

	private boolean extractCFPPage () {
		boolean success = true;
		builder.text ( "Extracting CFP page from <a href='http://www.wikicfp.com/cfp' target='_blank'>all CFP pages</a> gathered in a single pass. Gets all meta-data." )
				.subProgress ( 0 ).totalSubProgress ( cfpLinksWithCategories.keySet ().size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		int count = 0;
		for ( String link : cfpLinksWithCategories.keySet () ) {
			WebPage page = new WebPage ();
			page.setUrl ( link );
			page = client.getWebPage ( page );
			if ( client.isValidWebPage ( page ) ) {
				Cfp cfp = WikiCFPUtils.extractCfpFromPage (
						page.getContent (),
						cfpLinksWithCategories.get ( link ),
						link );
				cfpToPublish.add ( cfp );
			}
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			count++;
		}

		return success;
	}

	private boolean publishCFP () {
		boolean success = true;
		builder.text ( "Publish all extracted CFP to RESTful API. All checks are done on the server side to ensure uniqueness." )
				.subProgress ( 0 ).totalSubProgress ( cfpLinksWithCategories.keySet ().size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient sessionClient = SessionClient.create ( APIClient.REST_API );
		String sessionId = sessionClient.createSession ();
		CFPClient cfpClient = CFPClient.create ( APIClient.REST_API, sessionId );

		int count = 0;
		for ( Cfp cfp : cfpToPublish ) {
			String cfpId = cfpClient.post ( cfp );
			cfp.setId ( cfpId );
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			count++;
		}

		sessionClient.commitSession ();

		return success;
	}

	private boolean commitToConferences () {
		boolean success = true;
		builder.text ( "Commit all CFP extracted to Conferences to build links." )
				.subProgress ( 0 ).totalSubProgress ( cfpLinksWithCategories.keySet ().size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		ConferenceClient conferenceClient = ConferenceClient.create ( APIClient.REST_API );

		int count = 0;
		for ( Cfp cfp : cfpToPublish ) {
			conferenceClient.addCfp ( cfp );
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			count++;
		}

		return success;
	}
}
