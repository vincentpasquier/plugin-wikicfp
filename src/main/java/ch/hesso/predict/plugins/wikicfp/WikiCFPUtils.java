/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.wikicfp;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WikiCFPUtils {

	private static final String WIKICFP_ROOT =
			"http://www.wikicfp.com";

	private static final String START_PAGE =
			WIKICFP_ROOT + "/cfp/allcat";

	private static final String EVENT_PAGE =
			"/cfp/servlet/event.showcfp?eventid=";

	private static final Pattern EXTRACT_ACRONYM =
			Pattern.compile ( "^([A-z0-9\\-\\*]{1,10})( \\-|\\(|/| )" );

	public static final String FIELD_ABSTRACT_REGISTRATION_DUE = "Abstract Registration Due";

	public static final String FIELD_SUBMISSION_DEADLINE = "Submission Deadline";

	public static final String FIELD_NOTIFICATION_DUE = "Notification Due";

	public static final String FIELD_FINAL_VERSION_DUE = "Final Version Due";

	public static final SimpleDateFormat _format = new SimpleDateFormat ( "yyyy-mm-dd'T'hh:MM:ss" );

	/**
	 * @return
	 */
	public static Map<String, String> extractCategoriesLinks () {
		Map<String, String> links = new HashMap<> ();
		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		WebPage webPage = new WebPage ();
		webPage.setUrl ( START_PAGE );
		webPage = client.getWebPage ( webPage );
		if ( !client.isValidWebPage ( webPage ) ) {
			return links;
		}
		Document doc = Jsoup.parse ( webPage.getContent () );
		Elements table = doc.select ( "a[href^=/cfp/call?conference=]" );
		for ( Element category : table ) {
			String name = category.text ();
			String link = category.attr ( "href" ).replaceAll ( " ", "%20" );
			links.put ( name, WIKICFP_ROOT + link );
		}
		return links;
	}

	/**
	 * @param link
	 *
	 * @return
	 */
	public static Set<String> extractCFPLinks ( final String link ) {
		Set<String> cfpLinks = new HashSet<> ();
		String lastLinkLastPage = "";
		String lastLinkPage = "";
		int page = 1;
		boolean keep = true;
		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		do {
			WebPage webPage = new WebPage ();
			webPage.setUrl ( link + "&page=" + page );
			webPage = client.getWebPage ( webPage );
			if ( !client.isValidWebPage ( webPage ) ) {
				continue;
			}
			Document doc = Jsoup.parse ( webPage.getContent () );
			Elements links = doc.select ( "a[href^=/cfp/servlet/event.showcfp?eventid=]" );
			keep = links.size () != 0;

			if ( keep ) {
				for ( Element extractLink : links ) {
					String cfpLink = extractLink.attr ( "href" );
					cfpLink = WIKICFP_ROOT + cfpLink;
					cfpLinks.add ( cfpLink );
				}
				lastLinkPage = links.last ().attr ( "href" );
			}

			keep = !lastLinkPage.equals ( lastLinkLastPage );
			page++;
			lastLinkLastPage = lastLinkPage;
		} while ( keep );


		return cfpLinks;
	}

	public static Cfp extractCfpFromPage ( final String content,
																				 final Collection<String> strings,
																				 final String url ) {
		Cfp cfp = new Cfp ();
		cfp.setOrigin ( url );
		Set<String> keywords = new HashSet<> ();
		for ( String string : strings ) {
			String keyword = string.substring ( 0, 1 ).toUpperCase () + string.toLowerCase ().substring ( 1 );
			keywords.add ( keyword );
		}
		cfp.setKeywords ( keywords );

		Document doc = Jsoup.parse ( content );
		String acronym = doc.select ( "span[property=v:summary]" ).first ().attr ( "content" );
		acronym = acronym.replaceAll ( "([0-9]*| |\\-)", "" ).trim ().toLowerCase ();
		cfp.setAcronym ( acronym );

		Elements start = doc.select ( "span[property=v:startDate]" );
		if ( !start.isEmpty () ) {
			cfp.setStart ( start.first ().attr ( "content" ) );
		}
		Elements end = doc.select ( "span[property=v:endDate]" );
		if ( !end.isEmpty () ) {
			cfp.setEnd ( end.first ().attr ( "content" ) );
		}
		Elements location = doc.select ( "span[property=v:locality]" );
		if ( !location.isEmpty () ) {
			cfp.setLocation ( location.first ().attr ( "content" ) );
		}
		cfp.setTitle ( doc.select ( "span[property=v:description]" ).first ().text () );
		Elements dates = doc.select ( "span[property=v:summary]" );

		Map<Cfp.Deadline, String> deadlines = new HashMap<> ();
		for ( Element summary : dates ) {
			Element date = summary.parent ().select ( "span[property=v:startDate" ).first ();
			if ( date == null ) {
				continue;
			}
			String dateContent = date.attr ( "content" );
			switch ( summary.attr ( "content" ) ) {
				case FIELD_ABSTRACT_REGISTRATION_DUE:
					deadlines.put ( Cfp.Deadline.REGISTRATION, dateContent );
					break;
				case FIELD_SUBMISSION_DEADLINE:
					deadlines.put ( Cfp.Deadline.SUBMISSION, dateContent );
					break;
				case FIELD_NOTIFICATION_DUE:
					deadlines.put ( Cfp.Deadline.NOTIFICATION, dateContent );
					break;
				case FIELD_FINAL_VERSION_DUE:
					deadlines.put ( Cfp.Deadline.FINAL_VERSION, dateContent );
					break;
			}
		}
		cfp.setDeadlines ( deadlines );
		String urlContent = doc.select ( "span[property=dc:source]" ).first ().attr ( "content" );
		if ( !urlContent.equals ( "" ) ) {
			cfp.setWebsite ( urlContent );
		}
		cfp.setContent ( doc.select ( ".cfp" ).first ().html ().replaceAll ( "<br( /|)>", "\n" ).replaceAll ( "\\n+", "\n" ) );
		Elements allRelated = doc.select ( ".contsec > .cfp" ).select ( "a[href^=" + EVENT_PAGE + "]" );
		for ( Element related : allRelated ) {
			String relatedString = related.attr ( "abs:href" );
			cfp.getRelated ().add ( relatedString );
		}
		return cfp;
	}
}
