/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.wikicfp.tests;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.wikicfp.WikiCFPUtils;
import ch.hesso.predict.restful.WebPage;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestExtractWikiCFP {

	private final Multimap<String, String> cfpLinksWithCategories
			= HashMultimap.create ();

	@Test
	public void testExtractCategories () {
		Map<String, String> links = WikiCFPUtils.extractCategoriesLinks ();
		for ( Map.Entry<String, String> entry : links.entrySet () ) {
			Set<String> categoryLinks = WikiCFPUtils.extractCFPLinks ( entry.getValue () );
			for ( String link : categoryLinks ) {
				cfpLinksWithCategories.put ( link, entry.getKey () );
			}
		}

		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		for ( String link : cfpLinksWithCategories.keySet () ) {
			WebPage page = new WebPage ();
			page.setUrl ( link );
			client.getWebPage ( page );
			System.out.println ( link );
		}
		System.out.println ( cfpLinksWithCategories.keySet ().size () );
	}

}
