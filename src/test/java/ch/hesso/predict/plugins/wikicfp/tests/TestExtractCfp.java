/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.wikicfp.tests;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.wikicfp.WikiCFPUtils;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.WebPage;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestExtractCfp {

	@Test
	public void testExtractCfp () {
		String link = "http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=6986&copyownerid=7186";

		WebPageClient client = WebPageClient.create ( APIClient.REST_API );
		WebPage webPage = new WebPage ();
		webPage.setUrl ( link );
		webPage = client.getWebPage ( webPage );

		if ( client.isValidWebPage ( webPage ) ) {
			Cfp cfp = WikiCFPUtils.extractCfpFromPage ( webPage.getContent (), new ArrayList<String> (), link );
			System.out.println ( cfp );
		}
	}

}
